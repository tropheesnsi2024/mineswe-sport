import pygame as pg

pg.init()

class Menu2():
    
    def __init__(self, gagne, temps, difficulte):

        self.font = pg.font.Font("CherryBombOne-Regular.ttf", 100)
        self.gagne = gagne
        self.score = round(temps,1)
        self.difficulte = round(difficulte,1)

        self.replay_area = [20,600,480,180]
        self.quit_area = [520,600,480,180]


    def _afficher(self,window):
        if self.gagne:
            texte_gagne = self.font.render("VICTOIRE",True,"green")
        else:
            texte_gagne = self.font.render("DEFAITE",True,"red")
        texte_difficulte = self.font.render(f"Difficulté: {self.difficulte}",True,"purple")
        texte_temps= self.font.render(f"TEMPS: {self.score}",True,"purple")
        texte_rejouer = self.font.render("REJOUER",True,"white")
        texte_quitter = self.font.render("QUITTER",True,"red")
        window.fill((0,0,0))
        #pg.draw.rect(window,"pink",(20,600,480,180))
        #pg.draw.rect(window,"pink",(520,600,480,180))
        window.blit(texte_gagne,(260,100))
        window.blit(texte_difficulte,(200,300))
        window.blit(texte_temps,(200,400))
        window.blit(texte_rejouer,(20,600))
        window.blit(texte_quitter,(520,600))


        


    def utiliser(self,clock,window):
        """
            affiche le menu d'apres partie et permet au joueur de choisir de rejouer ou de quitter le jeu
        """


        #afficher txt gagné/perdu + difficulté grille

        #bouton rejouer ------ bouton quitter



        # graphic element to do later

        # self.test = pg.surfarray.array2d(window) 
        # for i in range(len(self.test)):
        #     for j in range(len(self.test[i])):
        #         self.test[i][j] = int("0000FFFF",16)
        # self.test = pg.surfarray.make_surface(self.test)

        dans_menu = True
        while dans_menu:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    exit()
                if event.type == pg.MOUSEBUTTONDOWN:
                    if event.dict["button"] == 1:
                        pos = pg.mouse.get_pos()
                        if pos[0] >= self.replay_area[0] and pos[0] <= self.replay_area[0] + self.replay_area[2] \
                        and pos[1] >= self.replay_area[1] and pos[1] <= self.replay_area[1] + self.replay_area[3]:
                            return 0
                        elif pos[0] >= self.quit_area[0] and pos[0] <= self.quit_area[0] + self.quit_area[2] \
                        and pos[1] >= self.quit_area[1] and pos[1] <= self.quit_area[1] + self.quit_area[3]:
                            return 1
            self._afficher(window)
            pg.display.update()
            clock.tick(60)
