from random import randint
from cell import Cell
from animation import Animation
import time

def lerp(a,b,coeff):
    return a + coeff * (b-a)

class Grid():

    def __init__(self, size, n_mines, cell_size) -> None:
        self.largeur = size
        self.hauteur = size
        self.n_bombe = n_mines
        self.grille = [[Cell(0,cell_size,(j,i)) for i in range(self.largeur)] for j in range(self.hauteur)]
        self.cell_size = cell_size
        #self.generate_grid(n_mines)
        self.n_mines = n_mines
        self.generated = False
        self.animations = []
        self.last_update = 0
        self.zeros_check = set()
        self.finito = False
        self.gagne = False

        self.data = []

    def placer_bombe(self, valeur_interdite):
        self.start = time.time()
        bombe = 0
        while bombe < self.n_bombe:
            x = randint(0, self.largeur - 1)
            y = randint(0, self.hauteur - 1)
            if not self.grille[y][x].est_bombe() and (x != valeur_interdite[1] or y != valeur_interdite[0]):
                self.grille[y][x].placer_bombe()
                self.incrementer_valeur_adjacente(x, y)
                bombe += 1
        self.add_data()

    def incrementer_valeur_adjacente(self, x, y):
        for i in range(max(0, x-1), min(self.largeur, x+2)):
            for j in range(max(0, y-1), min(self.hauteur, y+2)):
                self.grille[j][i].incrementer_valeur()

    def generate_grid(self, n_mines, forbidden_spot):
        forbidden_spot = [forbidden_spot[0],forbidden_spot[1]]  
        for i in range(n_mines):
            available_slots = []
            for j in range(self.largeur):
                for k in range(self.largeur):
                    if self.grille[j][k].valeur == 0 and [j,k] != forbidden_spot:
                        available_slots.append([j,k])
            where = randint(0, len(available_slots)-1)
            self.grille[available_slots[where][0]][available_slots[where][1]].valeur = -1

        for i in range(self.largeur):
            for j in range(self.largeur):
                if self.grille[i][j].valeur != 0:
                    continue
                directions = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]]
                total_count = 0
                for direction in directions:
                    if i + direction[0] >= 0 and i + direction[0] < self.largeur and j + direction[1] >= 0 and j + direction[1] < self.largeur:
                        if self.grille[i+direction[0]][j+direction[1]].valeur == -1:
                            total_count += 1
                self.grille[i][j].valeur = total_count

    def draw(self, window):
        for row in self.grille:
            for cell in row:
                cell.draw(window)

    def _getPos(self,pos):
        return (int(pos[0]/self.cell_size),int(pos[1]/self.cell_size))
    
    def _getCell(self,pos):
        return self.grille[pos[0]][pos[1]]

    def addAnimation(self, cells, pos, initial_color, final_color):
        self.animations.append([Animation(pos, self.cell_size, initial_color, final_color)])
        for cell in cells:
            cell.animation = self.animations[-1]
            cell.offset = [pos[0]//self.cell_size-cell.pos[0]/self.cell_size,pos[1]//self.cell_size-cell.pos[1]/self.cell_size]
            
            default_color0 = [255,0,0]
            default_color1 = [0,255,0]
            #print(final_color)
            cell.next_color = [lerp(default_color0[i],default_color1[i],min(final_color,1)) for i in range(3)]
        

    def updateAnimations(self, time):
        for animation in self.animations:
            animation[0].update()


    def zerosReveal(self,pos,iterations):
        if pos in self.zeros_check:
            return
        cell = self.grille[pos[0]][pos[1]]
        if cell.valeur == -1:
            return
        elif cell.valeur != 0:            
            if iterations != 1:
                cell.decouverte = True
                default_color0 = [255,0,0]
                default_color1 = [0,255,0]                
                cell.current_color = [lerp(default_color0[i],default_color1[i],min(cell.closeFlags/cell.valeur,1)) for i in range(3)]
            if iterations != 0:
                return
        if cell.valeur == 0 and iterations == 0:
            iterations += 1
        
        # case is zero
        cell.decouverte = True
        self.zeros_check.add(pos)
        dirs = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]]
        for dir in dirs:
            if pos[0]+dir[0] >= 0 and pos[0]+dir[0] < self.largeur and pos[1]+dir[1] >= 0 and pos[1]+dir[1] < self.largeur:
                self.zerosReveal((pos[0]+dir[0],pos[1]+dir[1]),iterations+1)

    def reveal(self, pos):
        coord = self._getPos(pos)
        cell = self.grille[coord[0]][coord[1]]
        if not self.generated:
            self.placer_bombe(coord)
            #self.generate_grid(self.n_mines,coord)
            self.generated = True
        if cell.drapeau:
            return
        if cell.decouverte:
            if cell.closeFlags == cell.valeur:
                dirs = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]]
                for dir in dirs:
                    if coord[0]+dir[0] >= 0 and coord[0]+dir[0] < self.largeur and coord[1]+dir[1] >= 0 and coord[1]+dir[1] < self.largeur:
                        if self.grille[coord[0]+dir[0]][coord[1]+dir[1]].decouverte == False and self.grille[coord[0]+dir[0]][coord[1]+dir[1]].drapeau == False:                            
                            self.reveal(((coord[0]+dir[0])*self.cell_size,(coord[1]+dir[1])*self.cell_size))

        if cell.valeur == -1:
            self.finito = True
        if cell.valeur != 0:
            cell.decouverte = True
            
        # case 0
        self.zerosReveal((coord[0],coord[1]),0)
        self.zeros_check = set()

        total_reveles = 0
        for i in range(self.largeur):
            for j in range(self.hauteur):
                if self._getCell((i,j)).decouverte:
                    total_reveles += 1
        if total_reveles == self.largeur*self.hauteur - self.n_bombe:
            self.finito = True
            self.gagne = True

    def flag(self, pos):
        coord = self._getPos(pos)
        cell = self._getCell(coord)
        if cell.decouverte:
            return
        cell.drapeau = not cell.drapeau
        dirs = [[-1,-1],[0,-1],[1,-1],[-1,0],[1,0],[-1,1],[0,1],[1,1]]
        animation_changes = []
        for dir in dirs:
            if coord[0] + dir[0] >= 0 and coord[0] + dir[0] < self.largeur and coord[1] + dir[1] >= 0 and coord[1] + dir[1] < self.largeur:
                if cell.drapeau:
                    new_cell = self._getCell([coord[0]+dir[0],coord[1]+dir[1]])
                    new_cell.closeFlags += 1
                    if new_cell.decouverte and cell.valeur != 0:
                        animation_changes.append((new_cell,new_cell.closeFlags,new_cell.valeur))
                else:
                    new_cell = self._getCell([coord[0]+dir[0],coord[1]+dir[1]])
                    new_cell.closeFlags -= 1
                    if new_cell.decouverte and cell.valeur != 0:
                        animation_changes.append((new_cell,new_cell.closeFlags,new_cell.valeur))

        while len(animation_changes):
            ratio =  animation_changes[0][1] / animation_changes[0][2]
            #print(animation_changes[0][2],animation_changes[0][1])
            initial_color = (animation_changes[0][1]-1) / (animation_changes[0][2]+0.001) if cell.drapeau else (animation_changes[0][1]+1) / (animation_changes[0][2]+0.001)
            #print(animation_changes[0][2]-1,"/",animation_changes[0][1],initial_color)
            index_update = [0]
            for i in range(1,len(animation_changes)):
                o_initial_color = (animation_changes[i][2]-1) / (animation_changes[i][1]+0.001) if cell.drapeau else (animation_changes[i][2]+1) / (animation_changes[i][1]+0.001)
                if animation_changes[i][2] / (animation_changes[i][1]+0.001) == ratio and initial_color == o_initial_color:
                    index_update.append(i)
            cell_update = []
            for i in range(len(index_update)-1,-1,-1):
                cell_update.append(animation_changes[i][0])
                animation_changes.pop(i)
            self.addAnimation(cell_update,pos, initial_color, ratio)
    











    def add_data(self):
        self.data = [
            self.avg_dist(),
            self.avg_dist_squared(),
            self.avg_cell_value(),
            self.alignement(),
            self.n_alignement(self.alignement()),
            self.alignement_acceptance(),
            self.biggest_z_zone(),
            self.n_z_zones(),
            self.biggest_z_zone() / self.n_z_zones(),
            self.biggest_m_zone(),
            self.biggest_m_zone() / self.n_bombe,
            self.biggest_1_zone(),
            self.bv3()
        ]



    # data
    def avg_dist(self):
        coord_mines = []
        for i in range(self.largeur):
            for j in range(self.hauteur):
                if self._getCell((i,j)).est_bombe():
                    coord_mines.append([i,j])
        total_distance = 0
        for i in range(len(coord_mines)):
            max_dist = 1000000
            for j in range(len(coord_mines)):
                if i == j:
                    continue
                dx = abs(coord_mines[i][0] - coord_mines[j][0])
                dy = abs(coord_mines[i][1] - coord_mines[j][1])
                max_dist = min(max_dist,max(dx,dy))
            total_distance += max_dist
        return total_distance / len(coord_mines)
    
    def avg_dist_squared(self):
        coord_mines = []
        for i in range(self.largeur):
            for j in range(self.hauteur):
                if self._getCell((i,j)).est_bombe():
                    coord_mines.append([i,j])
        total_distance = 0
        for i in range(len(coord_mines)):
            max_dist = 1000000
            for j in range(len(coord_mines)):
                if i == j:
                    continue
                dx = abs(coord_mines[i][0] - coord_mines[j][0])
                dy = abs(coord_mines[i][1] - coord_mines[j][1])
                max_dist = min(max_dist,max(dx**2,dy**2))
            total_distance += max_dist
        return total_distance / len(coord_mines)
    
    def avg_cell_value(self):
        total_value = 0
        n_values = 0
        for i in range(self.hauteur):
            for j in range(self.largeur):
                cell = self._getCell((i,j))
                if cell.est_bombe():
                    continue
                total_value += cell.valeur
                n_values += 1
        return total_value / n_values
    
    def alignement(self):
        biggest_alignement = 0
        #horizontal
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((i,j)).est_bombe():
                    current_alignement += 1
            biggest_alignement = max(biggest_alignement,current_alignement)
        #vertical
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((j,i)).est_bombe():
                    current_alignement += 1
            biggest_alignement = max(biggest_alignement,current_alignement)
        return biggest_alignement
    
    def n_alignement(self,alignement):
        n = 0
        #horizontal
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((i,j)).est_bombe():
                    current_alignement += 1
            if current_alignement == alignement:
                n+=1
        #vertical
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((j,i)).est_bombe():
                    current_alignement += 1
            if current_alignement == alignement:
                n += 1
        return n
    
    def alignement_acceptance(self):
        biggest_alignement = 0
        #horizontal
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((i,j)).est_bombe():
                    current_alignement += 1
                if i - 1 >= 0:
                    if self._getCell((i-1,j)).est_bombe():
                        current_alignement += 1
                if i + 1 < self.largeur:
                    if self._getCell((i+1,j)).est_bombe():
                        current_alignement += 1
            biggest_alignement = max(biggest_alignement,current_alignement)
        #vertical
        for i in range(self.hauteur):
            current_alignement = 0
            for j in range(self.largeur):
                if self._getCell((j,i)).est_bombe():
                    current_alignement += 1
                if i - 1 >= 0:
                    if self._getCell((j,i-1)).est_bombe():
                        current_alignement += 1
                if i + 1 < self.largeur:
                    if self._getCell((j,i+1)).est_bombe():
                        current_alignement += 1
            biggest_alignement = max(biggest_alignement,current_alignement)
        return biggest_alignement
    
    def biggest_z_zone(self):
        self.visited = set()
        bz= 0
        for i in range(self.hauteur):
            for j in range(self.largeur):
                bz = max(bz,self._biggest_z_d((i,j)))
        return bz
    
    def _biggest_z_d(self, coord):
        if coord in self.visited or self._getCell(coord).valeur != 0:
            return 0
        total_z = 1
        self.visited.add(coord)
        for i in range(max(0, coord[0]-1), min(self.largeur, coord[0]+2)):
            for j in range(max(0, coord[1]-1), min(self.hauteur, coord[1]+2)):
                if coord[0] == i and coord[1] == j:
                    continue
                total_z += self._biggest_z_d((i,j))
        return total_z
    
    def n_z_zones(self):
        self.visited = set()
        n = 0
        for i in range(self.hauteur):
            for j in range(self.largeur):
                if self._biggest_z_d((i,j)) != 0:
                    n += 1
        return n        
    
    def biggest_m_zone(self):
        self.visited = set()
        bz= 0
        for i in range(self.hauteur):
            for j in range(self.largeur):
                bz = max(bz,self._biggest_m_d((i,j)))
        return bz
    
    def _biggest_m_d(self, coord):
        if coord in self.visited or self._getCell(coord).valeur != -1:
            return 0
        total_z = 1
        self.visited.add(coord)
        for i in range(max(0, coord[0]-1), min(self.largeur, coord[0]+2)):
            for j in range(max(0, coord[1]-1), min(self.hauteur, coord[1]+2)):
                if coord[0] == i and coord[1] == j:
                    continue
                total_z += self._biggest_m_d((i,j))
        return total_z
    
    def biggest_1_zone(self):
        self.visited = set()
        bz= 0
        for i in range(self.hauteur):
            for j in range(self.largeur):
                bz = max(bz,self._biggest_1_d((i,j)))
        return bz
    
    def _biggest_1_d(self, coord):
        if coord in self.visited or self._getCell(coord).valeur != 1:
            return 0
        total_z = 1
        self.visited.add(coord)
        for i in range(max(0, coord[0]-1), min(self.largeur, coord[0]+2)):
            for j in range(max(0, coord[1]-1), min(self.hauteur, coord[1]+2)):
                if coord[0] == i and coord[1] == j:
                    continue
                total_z += self._biggest_1_d((i,j))
        return total_z
    

    def bv3(self):
        self.visited = []
        for i in range(self.largeur):
            for j in range(self.hauteur):
                self._bv3_search((i,j))
        #print(self.visited)
        for i in range(len(self.visited)-1,-1,-1):
            for j in range(max(0, self.visited[i][0]-1), min(self.largeur, self.visited[i][0]+2)):
                for k in range(max(0, self.visited[i][1]-1), min(self.hauteur, self.visited[i][1]+2)):
                    self.visited.append((j,k))

        total = 0
        for i in range(self.largeur):
            for j in range(self.hauteur):
                if (i,j) not in self.visited and  not self._getCell((i,j)).est_bombe():
                    total+=1
        total+= self.n_z_zones()
        return total

    def _bv3_search(self, coord):
        if coord in self.visited or self._getCell(coord).valeur != 0:
            return
        self.visited.append(coord)
        for i in range(max(0, coord[0]-1), min(self.largeur, coord[0]+2)):
            for j in range(max(0, coord[1]-1), min(self.hauteur, coord[1]+2)):
                if coord[0] == i and coord[1] == j:
                    continue
                self._bv3_search((i,j))
