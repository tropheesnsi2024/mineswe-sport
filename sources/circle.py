import time
import pygame as pg

class Cercle:

    def __init__(self, color, center, radius, width=0) -> None:
        self.age = time.time()
        self.color = color
        self.radius = radius
        self.center = center
        self.width = width
    def update(self):
        if self.radius > 500:
            return False
        self.radius += 17
        return True
    def draw(self, window, offset, cell_size):
        if self.radius < 1:
            return
        pg.draw.circle(window, self.color, (self.center[0]+offset[0]*cell_size,self.center[1]+offset[1]*cell_size),self.radius,self.width)