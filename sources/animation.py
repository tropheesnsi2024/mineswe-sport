from circle import Cercle

def lerp(a,b,coeff):
    return a + coeff * (b-a)

class Animation():

    def __init__(self, pos, cell_size, initial_color_r, final_color_r) -> None:
        self.circles = []
        if final_color_r > 1:
            final_color_r = 0
        initial_color_r = min(initial_color_r,1)

        default_color0 = [255,0,0]
        default_color1 = [0,255,0]

        initial_color = [lerp(default_color0[i],default_color1[i],initial_color_r) for i in range(3)]
        final_color = [lerp(default_color0[i],default_color1[i],final_color_r) for i in range(3)]

        for i in range(-1,-250,-1):
            #print(initial_color)
            self.circles.append(Cercle([int(lerp(initial_color[0], final_color[0],i/-250)),
                                        int(lerp(initial_color[1], final_color[1],i/-250)),
                                        int(lerp(initial_color[2], final_color[2],i/-250))
                                        ],(pos[0]%cell_size,pos[1]%cell_size),i))
        self.circles.append(Cercle(final_color,(pos[0] - cell_size*(pos[0]//cell_size),pos[1] - cell_size*(pos[1]//cell_size)),-250,0))

    def update(self):
        for i in range(len(self.circles)-1,-1,-1):
            if not self.circles[i].update():
                self.circles.pop(i)