import pygame as pg
import time
import sys

def gc(s):
    return (int(s[0:2],16),int(s[2:4],16),int(s[4:6],16))

def lerp(a,b,coeff):
    return a + coeff * (b-a)

def clamp(val,min_v,max_v):
    return min(max(min_v,val),max_v)

class Cell():

    

    def __init__(self, valeur,surf_size,pos) -> None:
        

        # consts
        self.OUTER_COLOR_OFFSET = 1
        self.OUTER_COLOR_WIDTH = 2

        # game data
        self.valeur = valeur
        self.decouverte = False
        self.drapeau = False
        self.closeFlags = 0

        # render data
        self.size = surf_size
        self.surf = pg.Surface((surf_size,surf_size),pg.SRCALPHA,32)
        self.surf.fill((255,255,255))
        self.inner_surf = pg.Surface((surf_size-2*self.OUTER_COLOR_OFFSET,surf_size-2*self.OUTER_COLOR_OFFSET), pg.SRCALPHA, 32)
        self.pos = (pos[0]*surf_size,pos[1]*surf_size)
        self.outer_color = (0,255,0)
        self.current_color = (255,0,0)
        self.next_color = (0,0,0)
        self.can_change = False
        self.animation = None
        self.anim_offset = 0

        self.inner_colors = [
            "darkgrey",          #0
            "aquamarine4",      #1
            "darkgoldenrod1", #2
            "chocolate1",     #3
            "crimson",        #4
            "darkred",        #5
            "gray0",          #6
            "indigo",         #7
            "magenta"         #8
        ]

        self.inner_colors = [
            "white",
            gc("71F579"),
            gc("e1f571"),
            gc("f5c271"),
            gc("f58d71"),
            gc("f57171"),
        ]

        #self.inner_colors = [
        #    gc("fffffc"),
        #    gc("ffc6ff"),
        #    gc("bdb2ff"),
        #    gc("a0c4ff"),
        #    gc("9bf6ff"),
        #    gc("caffbf"),
        #    gc("fdffb6"),
        #    gc("ffd6a5"),
        #    gc("ffadad")
        #]


        # TEMPORARY /!\
        self.font = pg.font.Font("CherryBombOne-Regular.ttf", 100)
        self.font_inter = pg.font.Font("CherryBombOne-Regular.ttf", 115)

    def est_bombe(self):
        return self.valeur == -1

    def placer_bombe(self):
        self.valeur = -1

    def incrementer_valeur(self):
        if self.valeur != -1:
            self.valeur += 1

    def drawAnimation(self, windoww):
        for circle in self.animation[0].circles:
            circle.draw(windoww, self.offset, self.size)
        if len(self.animation[0].circles) >= 7:
            self.can_change = True
        elif len(self.animation[0].circles) < 7 and self.can_change:
            self.current_color = self.next_color.copy()
            #print(self.current_color)
            self.can_change = False
            #print("kk")

    def draw(self,window):
        #clear
        self.surf.fill((120,120,120))
        
        #background
        pg.draw.rect(self.inner_surf,"grey",(0,0,1000,1000))
        
        #TEMPORARY STUFF
        if self.drapeau:
            self.inner_surf.fill((255,0,0))
            window.blit(self.surf, self.pos)
            window.blit(self.inner_surf, (self.pos[0]+self.OUTER_COLOR_OFFSET,self.pos[1]+self.OUTER_COLOR_OFFSET))
            return
        
        surf_inter = pg.Surface((self.size,self.size),pg.SRCALPHA, 32)
        #outer color
        default_color = (255,0,0)
        if self.decouverte and self.valeur != 0:
            pg.draw.rect(surf_inter, self.current_color, (10,10,self.size-2*10,self.size-2*10))
        
        if self.animation is not None:
            self.drawAnimation(surf_inter)

        if self.decouverte:
            #inner color
            pg.draw.rect(self.inner_surf, self.inner_colors[self.valeur], (self.OUTER_COLOR_WIDTH,self.OUTER_COLOR_WIDTH,self.size-2*self.OUTER_COLOR_OFFSET-2*self.OUTER_COLOR_WIDTH,self.size-2*self.OUTER_COLOR_OFFSET-2*self.OUTER_COLOR_WIDTH))
            if self.valeur != 0:
                text_mask = self.font_inter.render(str(self.valeur),False,"purple")
                self.inner_surf.blit(text_mask,(23,-32))

                text_surface = self.font.render(str(self.valeur),False,"black")
                self.inner_surf.blit(text_surface, (28,-20))
                self.inner_surf.set_colorkey("purple")
                self.inner_surf.convert_alpha()
        
        window.blit(self.surf,self.pos)
        #window.blit(self.inner_surf, (self.pos[0]+self.OUTER_COLOR_OFFSET,self.pos[1]+self.OUTER_COLOR_OFFSET))
        window.blit(surf_inter,self.pos)
        window.blit(self.inner_surf,self.pos)