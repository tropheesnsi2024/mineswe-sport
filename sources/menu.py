import pygame as pg
from math import sqrt

class Menu():

    def __init__(self, w_size, min, max, window, data, time) -> None:
        self.w_size = w_size
        self.min = min
        self.max = max
        self.window = window
        self.data = data
        
        self.data.append(time)

        #renderer stuff
        self.circle_pos = [self.w_size / 2,self.w_size / 2]
        self.min_c = self.w_size / 10
        self.max_c = self.w_size * 0.9

        #physics stuff
        self.circle_moving = False

        # both lol
        self.submit_area = (0.4*self.w_size,0.8*self.w_size,0.2*self.w_size,0.1*self.w_size)
    
    def draw(self):
        pg.draw.circle(self.window,(255,0,0),self.circle_pos,30)

    def move_circle(self, pos):
        if self.circle_moving:
            self.circle_pos[0] = max(self.min_c,min(self.max_c,pos[0]))

    def is_in_circle(self,pos):
        distance = sqrt((pos[0] - self.circle_pos[0])**2 + (pos[1] - self.circle_pos[1])**2) - 30
        if distance < 0.1:
            self.circle_moving = True

    def is_submit(self, pos):
        if pos[0] >= self.submit_area[0] and pos[0] <= self.submit_area[0] + self.submit_area[2] and \
        pos[1] >= self.submit_area[1] and pos[1] <= self.submit_area[1] + self.submit_area[3]:
            return True
        return False

    def use(self, clock):
        quoicoubeh = True
        while quoicoubeh:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    pg.quit()
                    exit()
                elif event.type == pg.MOUSEBUTTONDOWN:
                    if event.dict["button"] == 1:
                        if self.is_submit(pg.mouse.get_pos()):
                            quoicoubeh = False
                            ratio = (self.circle_pos[0]- (0.1*self.w_size)) / 0.8*self.w_size
                            a = self.circle_pos[0]
                            b = 0.1*self.w_size
                            c = 0.8*self.w_size
                            #print(a, b, c, " ->", (a-b)/c)
                            #print(f"({self.circle_pos[0]} - {0.1*self.w_size}) / {0.8*self.w_size} = {ratio}")
                            data_t = ""
                            for elem in self.data:
                                data_t += str(elem) + ";"
                            with open("output.txt",'a') as bass:
                                bass.write(f"\n{(a-b)/c};{data_t}".replace(".",",")) # data
                        self.is_in_circle(pg.mouse.get_pos())
                elif event.type == pg.MOUSEBUTTONUP:
                    if event.dict["button"] == 1:
                        self.circle_moving = False
                elif event.type == pg.MOUSEMOTION:
                    self.move_circle(pg.mouse.get_pos())
            pg.draw.rect(self.window,(0,0,0),((0,0),(self.w_size,self.w_size)))
            pg.draw.rect(self.window,(255,255,255),((self.min_c,self.w_size/2),(0.8*self.w_size,10)))
            pg.draw.rect(self.window,(0,255,2),self.submit_area)
            self.draw()
            pg.display.update()
            clock.tick(60)
